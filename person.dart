import 'package:ymoney_app/core/domain/models/account.dart';
import 'package:ymoney_app/core/domain/models/beneficiary.dart';
import 'package:ymoney_app/core/domain/models/country.dart';
import 'package:ymoney_app/core/domain/models/financial_institution.dart';
import 'package:ymoney_app/core/domain/models/kyc_document.dart';
import 'package:ymoney_app/core/domain/models/phone_number.dart';
import 'package:ymoney_app/core/domain/models/ymo_card.dart';
import 'package:ymoney_app/core/util/enums.dart';
import 'package:ymoney_app/feature_home/domain/models/address.dart';
import 'package:ymoney_app/feature_home/domain/models/employment_status.dart';
import 'package:ymoney_app/feature_home/domain/models/industry_type.dart';

class Person {
  final String id;
  final String? firstName;
  final String? lastName;
  final String? name;
  final String? relationShip;
  final DateTime? birthDate;
  final String? email;
  final PhoneNumber? phoneNumber;
  final String? customerNumber;
  final FinancialInstitution? financialInstitution;
  final String? referrerCode;
  final String? referralCode;
  final String? qrcodeData;
  Account? account;
  final EnumPersonStatus? status;
  final List<Beneficiary>? beneficiaries;
  final Address? address;
  final String? cityOfBirth;
  final EmploymentStatus? employmentStatus;
  final IndustryType? industry;
  final int? organizationNumber;
  final int? pendingOutgoingTransferRequests;
  final int? pendingIncomingTransferRequests;
  final String? citizenship;
  final String? birthPlace;
  final Country? birthCountry;
  final Country? nationality;
  final String? gender;
  final double? estimatedAmountTransferByMonth;
  final String? otherEmploymentStatus;
  final String? otherIndustry;
  final Country? country;
  final bool? hasPendingIdentityDocument;
  final bool? hasIdentityDocument;
  final List<KycDocument>? kycDocuments;

  List<YmoCard>? ymoCards;

  Person({
    required this.id,
    this.firstName,
    this.lastName,
    this.name,
    this.relationShip,
    this.birthDate,
    this.email,
    this.phoneNumber,
    this.customerNumber,
    this.financialInstitution,
    this.referrerCode,
    this.referralCode,
    this.qrcodeData,
    this.account,
    this.status,
    this.beneficiaries,
    this.address,
    this.cityOfBirth,
    this.employmentStatus,
    this.industry,
    this.organizationNumber,
    this.pendingOutgoingTransferRequests,
    this.pendingIncomingTransferRequests,
    this.citizenship,
    this.birthPlace,
    this.birthCountry,
    this.nationality,
    this.gender,
    this.estimatedAmountTransferByMonth,
    this.otherEmploymentStatus,
    this.otherIndustry,
    this.country,
    this.hasPendingIdentityDocument,
    this.hasIdentityDocument,
    this.ymoCards,
    this.kycDocuments,
  });

  factory Person.empty() => Person(id: '');

  factory Person.example() => Person(
      id: 'e974f24c-69a8-47d6-90fc-ad483e639758',
      firstName: 'Fatoumata',
      lastName: 'Diallo',
      birthDate: DateTime(1985, 6, 15),
      email: 'test@gmail.com',
      beneficiaries: []);
}
